<?php
session_start();

require 'config.php';

if (isset($_SESSION['banco']) && !empty($_SESSION['banco'])) {
    $id = $_SESSION['banco'];

    $sql = $pdo->prepare("SELECT * FROM contas WHERE id = :id");
    $sql->bindValue(":id", $id);
    $sql->execute();

    if ($sql->rowCount() > 0) {
        $info = $sql->fetch();
        $info['saldo'] = floatval($info['saldo']);
    } else {
        header("Location: login.php");
        exit;
    }
} else {
    header("Location: login.php");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Caixa Eletrônico</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>

    <h1>Banco XYZ</h1>

    Titular: <?php echo $info['titular']; ?> <br><br>

    Agência: <?php echo $info['agencia']; ?> || Conta: <?php echo $info['conta']; ?> || <a href="logout.php">Sair</a>
    <br><br>

    Saldo: <?php echo $info['saldo']; ?> <br>
    <hr><br>

    <h3>Movimentação/Extrato</h3>

    <a href="add-transacao.php">Adicionar Transação</a><br/><br/>

    <table class="table table-hover">
        <thead>
            <tr>
                <th>Data/hora</th>
                <th>Valor</th>
            </tr>
        </thead>

        <?php
$sql = $pdo->prepare("SELECT * FROM historico WHERE id_conta = :id_conta");
$sql->bindValue(":id_conta", $id);
$sql->execute();

if ($sql->rowCount() > 0) {
    foreach ($sql->fetchAll() as $item) {
        ?>
        <tbody>
            <tr>
                <td>
                    <?php echo date('d/m/Y H:i:s', strtotime($item['data_operacao'])); ?>
                </td>
                <td>
                    <?php if ($item['tipo'] == '0'): ?>
                    <font color="green">R$ <?php echo $item['valor']; ?></font>
                    <?php else: ?>
                    <font color="red">- R$ <?php echo $item['valor']; ?></font>
                    <?php endif;?>
                </td>
            </tr>
        </tbody>
        <?php
}
}
?>
    </table>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>

</body>

</html>